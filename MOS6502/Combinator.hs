module MOS6502.Combinator where

import MOS6502.Type

forMap :: [Byte -> S ()] -> Byte -> S ()
forMap fs r = mapM_ ($ r) fs
