module MOS6502.Flag where

import Control.Lens
import Data.Bits.Lens
import MOS6502.Type

n :: L Bit
n = p.bitAt 7

v :: L Bit
v = p.bitAt 6

b :: L Bit
b = p.bitAt 4

d :: L Bit
d = p.bitAt 3

i :: L Bit
i = p.bitAt 2

z :: L Bit
z = p.bitAt 1

c :: L Bit
c = p.bitAt 0

nCheck :: Byte -> S ()
nCheck r = n .= r^.bitAt 7

zCheck :: Byte -> S ()
zCheck r = z .= (r == 0)
