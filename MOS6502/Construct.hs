module MOS6502.Construct where

import Data.Array.Unboxed
import MOS6502.Type

-- $setup
-- >>> import Control.Lens

cpu :: CPU
cpu = CPU
  { _a = 0
  , _x = 0
  , _y = 0
  , _p = 0
  , _s = 0
  , _pc = 0
  , _cy = 0
  , _mm = listArray (0, 65535) $ replicate 66536 0 }

{- |
>>> let x = cpu
>>> x^?!ram.ix 0
0

>>> x^?!ram.ix 65535
0
-}
