{-# LANGUAGE RankNTypes #-}

module MOS6502.Addressing where

import Control.Lens
import Control.Monad
import MOS6502.Type

useBy :: Integral a => L a -> S Byte
useBy r = do
  j <- uses r fromIntegral
  use $ mm.singular (ix j)

nx1 :: S Byte
nx1 = do
  j <- useBy pc
  pc += 1
  return j

nx2 :: S DByte
nx2 = liftM2 toDByte nx1 nx1

(@+) :: (Integral a, Integral b) => S a -> L b -> S a
j @+ k = liftM2 (\q r -> q + fromIntegral r) j (use k)

imm :: S DByte
imm = liftM fromIntegral nx1

zp :: S DByte
zp = cy += 1 >> imm

zpx :: S DByte
zpx = cy += 2 >> imm @+ x

ab :: S DByte
ab = cy += 2 >> nx2

aplus :: L Byte -> Bool -> S DByte
aplus r penalty = do
  cy += 3
  j <- nx2
  k <- uses r ((+ j) . fromIntegral)
  when (penalty && page j /= page k) (cy += 1)
  return k

abx :: Bool -> S DByte
abx = aplus x

aby :: Bool -> S DByte
aby = aplus y

-- | Relative addressing is used exclusively in branching operations.
-- A Byte is added to the PC. 
rel :: Byte -> S ()
rel r = do
  cy += 1
  l <- uses pc page
  j <- pc <+= fromIntegral r
  when (l /= page j) (cy += 1)
