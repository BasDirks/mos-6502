{-# LANGUAGE RankNTypes #-}

module MOS6502.Read where

import Control.Lens hiding (op, set, enum)
import Control.Monad
import MOS6502.Addressing
import MOS6502.Combinator
import MOS6502.Flag
import MOS6502.Type

checked :: Byte -> S ()
checked = forMap [nCheck, zCheck]

go :: S ()
go = nx1 >>= op >> cy += 2 >> go

branch :: (Bool -> S () -> S ()) -> L Bit -> S ()
branch f r = do
  j <- useBy pc
  k <- use r
  f k (rel j)

transfer :: L Byte -> L Byte -> S ()
transfer q r = uses q fromIntegral >>= (r <.=) >>= checked

enum :: (Byte -> Byte) -> DByte -> S ()
enum f r = mm.singular (ix r) <%= f >>= checked >> cy += 3

inc, dec :: DByte -> S ()
inc = enum succ
dec = enum pred

enumR :: (Byte -> Byte) -> L Byte -> S ()
enumR f r  = r <%= f >>= checked

incR, decR :: L Byte -> S ()
incR = enumR succ
decR = enumR pred

lda :: DByte -> S ()
lda r = do
  j <- use mm
  a <.= j^?!ix r >>= checked

op :: Byte -> S ()

-- Clear flag
op 0x18 = c .= False
op 0xD8 = d .= False
op 0x58 = i .= False
op 0xB8 = v .= False

-- Set flag
op 0x38 = c .= True
op 0xF8 = d .= True
op 0x78 = i .= True

-- Branch if clear
op 0x90 = branch unless c
op 0xD0 = branch unless z
op 0x10 = branch unless n
op 0x70 = branch unless v

-- Branch if set
op 0xB0 = branch when c
op 0xF0 = branch when z
op 0x30 = branch when n
op 0x50 = branch when v

-- Transfer
op 0xAA = transfer a x
op 0xA8 = transfer a y
op 0xBA = transfer s x
op 0x8A = transfer x a
op 0x9A = transfer x s
op 0x98 = transfer a y

-- Decrement
op 0xC6 = dec =<< zp 
op 0xD6 = dec =<< zpx
op 0xCE = dec =<< ab 
op 0xDE = dec =<< abx noPP

op 0xCA = decR x
op 0x88 = decR y

-- Increment
op 0xE6 = inc =<< zp
op 0xF6 = inc =<< zpx
op 0xEE = inc =<< ab
op 0xFE = inc =<< abx noPP

op 0xE8 = incR x
op 0xC8 = incR y

-- Load
op 0xA9 = lda =<< imm
op 0xA5 = lda =<< zp
op 0xB5 = lda =<< zpx
op 0xAD = lda =<< ab
op 0xBD = lda =<< abx withPP
op 0xB9 = lda =<< aby withPP

op _ = return ()
