{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE RankNTypes #-}

module MOS6502.Type where

import Control.Lens
import Control.Monad.State
import Data.Array.Unboxed
import Data.Bits.Bitwise
import Data.Word
import Prelude hiding (splitAt)

type Bit    = Bool
type Cycle  = Word
type Byte   = Word8
type DByte  = Word16
type Memory = Array DByte Byte
type S a    = State CPU a
type L a    = Lens' CPU a

data CPU = CPU
  { _cy :: Cycle  -- Cycle Count
  , _a  :: Byte   -- Accumulator
  , _x  :: Byte   -- Register X
  , _y  :: Byte   -- Register Y
  , _p  :: Byte   -- Progmm Flags
  , _s  :: Byte   -- Stack Pointer
  , _pc :: DByte  -- Progmm Counter
  , _mm :: Memory -- Random Access Memory
  } deriving Show

makeLenses ''CPU

withPP, noPP :: Bool
withPP = True
noPP = False

toDByte :: Byte -> Byte -> DByte
toDByte j k = fromListLE $ toListLE j ++ toListLE k

fromDByte :: DByte -> (Byte, Byte)
fromDByte j = let (k, l) = splitAt 8 j in (fromIntegral l, fromIntegral k)

page :: DByte -> Byte
page = fromIntegral . (`div` 0x100)

